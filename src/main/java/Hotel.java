import java.io.*;
import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created: 16.02.2023
 *
 * @author Tolga Estelik {tolga}
 */
public class Hotel {

    private String name;
    private String location;
    private int size;
    private boolean smoking;
    private int rate;
    private LocalDate date;
    private String owner;


    public Hotel(byte[] data, Map<String, Short> colums) {
        ByteBuffer buffer = ByteBuffer.wrap(data);

        for (Map.Entry<String, Short> entry : colums.entrySet()) {
            String columnName = entry.getKey();
            short columnLength = entry.getValue();

            byte[] columnData = new byte[columnLength];
            buffer.get(columnData);

            String columnValue = new String(columnData);
            switch (columnName) {
                case "name" -> this.name = columnValue.trim();
                case "location" -> this.location = columnValue.trim();
                case "size" -> this.size = Integer.parseInt(columnValue.trim());
                case "smoking" -> this.smoking = (columnValue.trim().equals("Y"));
                case "rate" -> this.rate = Integer.parseInt(columnValue.trim().split("\\$")[1].replace(".", ""));
                case "date" -> this.date = parseDate(columnValue.trim());
                case "owner" -> this.owner = columnValue.trim();
                default -> throw new IllegalArgumentException("Unknown column name: " + columnName);
            }
        }
    }

    public Hotel(String name, String location, int size, boolean smoking, int rate, LocalDate date, String owner) {
        this.name = name;
        this.location = location;
        this.size = size;
        this.smoking = smoking;
        this.rate = rate;
        this.date = date;
        this.owner = owner;

    }

    public static Map<String, Short> readColumns(String filename) throws IOException {
        Map<String, Short> byteMap = new LinkedHashMap<>();
        try (DataInputStream dataHotel = new DataInputStream(new FileInputStream(filename))) {
            int id = dataHotel.readInt();
            int offset = dataHotel.readInt();
            short colums = dataHotel.readShort();
            for (int i = 0; i < colums; i++) {

                short stringLenght = dataHotel.readShort();
                byte[] byteArr = new byte[stringLenght];

                dataHotel.read(byteArr);
                String as = new String(byteArr);
                byteMap.put(as, dataHotel.readShort());
            }
        } catch (FileNotFoundException e) {
            System.err.println("Fehler beim Öffnen der File!" + e);
        }
        return byteMap;
    }

    public static int getStartingOffset(String filename) throws IOException {
        int offset = 0;
        try (DataInputStream hotelStream = new DataInputStream(new FileInputStream(filename))) {
            int id = hotelStream.readInt();
            offset += hotelStream.readInt();
        } catch (FileNotFoundException e) {
            System.err.println("Fehler beim öffnen der Datei!" + e);
        }
        return offset;
    }

    public static Set<Hotel> readHotels(String filename) throws IOException {
        SortedSet<Hotel> hotelSet = new TreeSet<>();
        try (DataInputStream hotelStream = new DataInputStream(new FileInputStream(filename))) {
            int id = hotelStream.readInt();
            int offset = hotelStream.readInt();
            hotelStream.skipBytes(offset - 8);
            while (hotelStream.available() > 0) {
                int deleted = hotelStream.readUnsignedShort();
                if (deleted == 0x0000) {
                    byte[] arr = new byte[159];
                    hotelStream.read(arr);
                    hotelSet.add(new Hotel(arr, readColumns(filename)));
                } else if (deleted == 0x8000) {
                    hotelStream.skipBytes(159);
                } else {
                    throw new IllegalArgumentException(filename);
                }

            }

        } catch (EOFException e) {
            throw new IllegalArgumentException(filename);

        } catch (FileNotFoundException e) {
            System.err.println("Fehler beim Öffnen der Datei" + e);

        }
        return hotelSet;
    }

    public static LocalDate parseDate(String stringDate) {
        String pattern = "yyyy/MM/dd";
        DateTimeFormatter df = DateTimeFormatter.ofPattern(pattern);

        return LocalDate.parse(stringDate, df);
    }

    @Override
    public int compareTo(Hotel o) {
        int compare = this.location.compareTo(o.location);

        if (compare == 0){
            return this.name.compareTo(o.name);

        }
        return Integer.compare(compare, 0);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return size == hotel.size && smoking == hotel.smoking && rate == hotel.rate && name.equals(hotel.name) && location.equals(hotel.location) && date.equals(hotel.date) && owner.equals(hotel.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, location, size, smoking, rate, date, owner);
    }
}



}
